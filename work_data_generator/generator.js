import {faker} from '@faker-js/faker'
import {connectionDB, executeStatement} from "../server/db/connection_db2.js"


/*----------------------------------------*
 |                                        |
 |              Employe                   |
 |                                        |
 *--------------------------------------- */

 
function createEmploye() {
    var employe = {}
    employe.prenom = faker.name.firstName()
    employe.nom =  faker.name.lastName()
    employe.telephone = faker.phone.number('501-###-###')
    employe.mail = faker.internet.email(employe.prenom, employe.lastName)
    employe.adresse = faker.address.streetAddress() //+ ' ' + faker.address.cityName() + ', ' + faker.address.country()
    employe.heuresParSemaine = parseInt((Math.random() > 0.95)?28:36)
    employe.congePayeRestant = parseInt(Math.random() *  51)
    employe.heuresSup = parseInt(Math.random() * 31)

    console.log("Employe created: ", employe)
    insertEmploye(employe)
    return employe
}


function insertEmploye(employe) {
    var query = `
    INSERT INTO employe (prenom, nom, telephone, mail, adresse, heuresParSemaine, congePayeRestant, heuresSup)
    VALUES (:prenom, :nom, :telephone, :mail, :adresse, :heuresParSemaine, :congePayeRestant, :heuresSup);`

    executeStatement(null, query, employe, function(){
        console.log("employe inserted")
    }) 
}

function employeAsignEntreprise(idEmploye, idEntreprise) {
    connectionDB.query(`
    UPDATE employe
    SET idEntreprise=:idEntreprise
    WHERE id=idEmploye
    `
    [idEntreprise, idEmploye],
    function(error, results) {
        if(error) {
            console.log("An error happen during the insertion of the following employe : ", employe)
        }
        return results
     }
    )
}

function employeAsignResponsableInterne(idEmploye, idResponsableInterne) {
    connectionDB.query(`
    UPDATE employe
    SET idResponsableInterne=:idResponsableInterne
    WHERE id=:idEmploye
    `
    [idResponsableInterne, idEmploye],
    function(error, results) {
        if(error) {
            console.log("An error happen during the insertion of the following employe : ", employe)
        }
        return results
     }
    )
}

function employeAsignEntrepriseAndResponsableInterne(idEmploye, idEntreprise, idResponsableInterne) {
    connectionDB.query(`
    UPDATE employe
    SET idEntreprise=:idEntreprise, idResponsableInterne=:idResponsableInterne
    WHERE id=:idEmploye
    `
    [idEntreprise, idResponsableInterne, idEmploye],
    function(error, results) {
        if(error) {
            console.log("An error happen during the insertion of the following employe : ", employe)
        }
        return results
     }
    )
}
/*----------------------------------------*
 |                                        |
 |              Entreprise                |
 |                                        |
 *--------------------------------------- */

 function createEntreprise() {
    var entreprise = {}
    entreprise.nom = faker.company.name()
    entreprise.telephone = faker.phone.number('501-###-###')
    entreprise.mail = faker.internet.email(entreprise.nom)
    entreprise.adresse = /*faker.address.streetAddress() + ' ' +*/ faker.address.cityName() //+ ', ' + faker.address.country()

    console.log("Entreprise created: ", entreprise)
    insertEntreprise(entreprise)
    return entreprise
}

function insertEntreprise(entreprise) {
    var query = `
        INSERT INTO entreprise(nom, telephone, mail, adresse)
        VALUES(:nom, :telephone, :mail, :adresse);`

    executeStatement(null, query, entreprise, function(){
        console.log("entreprise inserted")
    }) 
}

/*----------------------------------------*
 |                                        |
 |              Projet                    |
 |                                        |
 *--------------------------------------- */

 function createProject() {
    var projet = {}
    projet.nom = faker.company.catchPhraseNoun()
    projet.debut = faker.date.recent(50)
    projet.finEstimee = faker.date.recent(20, projet.debut)
    projet.nbPersonnesRequises = parseInt(Math.random() * 101) + 10

    console.log("Projet created: ", projet)
    insertProjet(projet)
    return projet
 }
 
function insertProjet(projet) {
    var query = `
        INSERT INTO projet(nom, debut, finEstimee, nbPersonnesRequises)
        VALUES(:nom, :debut, :finEstimee, :nbPersonnesRequises);`

    executeStatement(null, query, projet, function(){
        console.log("projet inserted")
    }) 
}

function projetAsignClient(idProjet, idClient) {
    connectionDB.query(`
    UPDATE projet
    SET idClient=:idClient
    WHERE id=:idProjet
    `
    [idClient, idProjet],
    function(error, results) {
        if(error) {
            console.log("An error happen during the insertion of the following employe : ", employe)
        }
        return results
     }
    )
 }


 function generateData(){

    for(let i=0; i<10; i++){
        createEmploye()
        createEntreprise()
        createProject()
    }
 }

 generateData()