#pip install pyexcel-ods3
from pyexcel_ods3 import save_data, get_data
from collections import OrderedDict
import json
from datetime import datetime, timedelta
#pip install mysql-connector-python
import mysql.connector
import random
#pip install names
import names
import sys

#Info connexion, pour l'instant ici
hote = "localhost"
utilisateur = "ian"
mdp = "password"
#Connexion BDD pour récupérer infos de projets pour générer les tâches
try :
     bdd = mysql.connector.connect(host=hote, user = utilisateur, password=mdp, database = "dds")
     curseur = bdd.cursor()
     curseur.execute("SELECT id, nom FROM projet")
     projets = curseur.fetchall()
except Exception as e:
     print(e)


     
def ligne1(datedepart, datefin):
     """Renvoie une liste de lignes.
     Par défaut, si datedepart est None, génère une feuille de temps sur une semaine avant la datefin.
     Si datefin est None, datefin est la date actuelle."""
     semaine = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"]
     heures = [f"{i}:00" for i in range(8,18)]
     if datefin == None :
          datefin = datetime.now().date()
     if datedepart == None:
          datedepart = datefin - timedelta(days = 7)

     date_actuelle = datedepart
     lignes = []
     for i in range((datefin - datedepart).days + 1):
          jour_actuel = semaine[date_actuelle.weekday()]
          for j in range(len(heures)-1):
               ligne = [str(date_actuelle), jour_actuel]
               if random.random() < 0.5 :
                    ligne.append(heures[j])
                    if j == len(heures) - 2 :
                         duree = 1
                    else :
                         duree = random.randint(1,2)
                    ligne.append(heures[j + duree])
                    #Salaire horaire de la prestation généré entre 15 et 30.
                    #Salaire moyen en France (horaire) autour de 18€
                    ligne.append(f"{random.randint(15,30)*duree}.00")

                    #Choix d'un projet sur lequel travailler de manière aléatoire
                    ligne.append(random.choice(projets)[1])
                    ligne.append("")
                    ligne.append("")
                    lignes.append(ligne.copy())
          date_actuelle = date_actuelle + timedelta(days = 1)
     return lignes


def version1(datedepart = None, datefin = None):
     """Génère un dictionnaire décrivant une feuille de temps de format 1.
     Arguments optionnels pour décrire la fenêtre de temps de la feuille de temps.
     Si pas d'arguments, une semaine de temps sera générée."""

     #Ici génération d'employé aléatoire, lecture dans la BDD potentielle à l'avenir
     curseur = bdd.cursor()
     curseur.execute("SELECT prenom, nom, idResponsableInterne FROM employe")
     liste_employes = curseur.fetchall()
     curseur.close()
     employe = random.choice(liste_employes)
     #employe = ( names.get_first_name(),names.get_last_name())
     if employe[-1] != None :
          curseur = bdd.cursor()
          curseur.execute(f"SELECT prenom, nom FROM employe where id = {employe[-1]}")
          responsable = curseur.fetchone()
     else :
          responsable = ('','')
          pass
     feuille_tmp = [
          ["Nom", employe[1]],
          ["Prénom", employe[0]],
          [],
          ["Responsable", responsable[0] + " " + responsable[1]],
          [],
          ["Jour", '', "Heure début", "Heure fin", "Montant facturable", "Projet", "Description", "Notes"]
    ]
     dico_tmp = {"Feuille 1" : feuille_tmp}
     lignes = ligne1(datedepart, datefin)
     for ligne in lignes : feuille_tmp.append(ligne)
     return dico_tmp

    
if __name__=="__main__":

     #Choix du fichier
     fichier = "testfile.ods" if len(sys.argv) < 2 else sys.argv[1]
     dico_tmp = version1(None, None)

     data = OrderedDict()
     
     #Ecriture dans data
     data.update(dico_tmp)
     #Ecriture dans le fichier
     save_data(fichier, data)
