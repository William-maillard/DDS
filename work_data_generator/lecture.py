#pip install pyexcel-ods3
from pyexcel_ods3 import get_data
from collections import OrderedDict
import json
#pip install mysql-connector-python
import mysql.connector
import sys


#Info connexion, pour l'instant ici
hote = "localhost"
utilisateur = "ian"
mdp = "password"
#Connexion BDD pour récupérer infos de projets pour générer les tâches
try :
     bdd = mysql.connector.connect(host=hote, user = utilisateur, password=mdp, database = "dds")
except Exception as e:
     print(e)
#info à ajouter
taches = []
taches_employe = []


def calculDuree(debut, fin):
     tabDebut = debut.split(":")
     tabFin = fin.split(":")

     tabDuree = [0, 0]

     tabDuree[0] = int(tabFin[0]) - int(tabDebut[0])

     if(tabDuree[0] < 0):
          tabDuree[0] = 24 + tabDuree[0]

     tabDuree[1] = int(tabFin[1]) - int(tabDebut[1])

     if(tabDuree[1] < 0):
          tabDuree[1] = 60 + tabDuree[1]

     duree = str(tabDuree[0])+":"

     if(tabDuree[0]<10):
          duree = "0" + duree 

     if(tabDuree[1]<10):
          duree = duree + "0" + str(tabDuree[1])
     else :
          duree = duree + str(tabDuree[1])

     return duree


def version2(data):
     """
     Lis les fichiers de format 2.
     Récupère les données à insérer
     """
     global tache, tache_employe
     for k,v in data.items():
          nom = v[1][1]
          prenom = v[0][1]
          try:
               responsable = v[3][1]
          except :
               responsable = ""
          #Récupération de l'id de l'employé avec son nom et prénom
          curseur = bdd.cursor()
          sql = "SELECT id, prenom, nom FROM employe WHERE prenom=%s AND nom=%s"
          val = (prenom, nom)
          curseur.execute(sql, val)
          
          row = curseur.fetchone()


          if row == None: #si l'employé n'est pas dans la table
               print("ERREUR : ")
               print("L'employé '"+prenom+" "+nom+"' n'existe pas dans la table employé.")
               quit()

          
          idEmploye = row[0] #on récupère l'id de l'employé

          curseur.execute("SELECT id FROM tache WHERE id=(SELECT max(id) FROM tache)")
          
          row = curseur.fetchone()
          if row == None:
               idTache = 1
          else : idTache = row[0] +1 #on récupère l'id de la dernière tache
        

          for i in range(7, len(v)):
               for j in range(int((len(v[6]) - 2) / 5)):
                    nomProjet = v[5][j*5 + 2]
                    try : # on récupère l'id du projet
                         sql = "SELECT id FROM projet WHERE nom=%s;"
                         val = (nomProjet,)
                         curseur.execute(sql, val)
                         row = curseur.fetchone()
                         idProjet = row[0]

                    except : 
                         idProjet = None
                         print("WARNING : Le projet '"+nomProjet+"' n'existe pas dans la table projet, pas d'insertion.")

                    try : description = v[i][j*5 + 3 + 2]
                    except : description = None
                    try: commentaire = v[i][j*5 + 4 + 2]
                    except : commentaire = None
                    taches.append(
                         {
                              "idProjet" : idProjet,
                              "coutEstime" : float(v[i][4]),
                              "dureeEstime" : calculDuree(v[i][j*5 + 2], v[i][j*5 + 3])
                         }
                    )
                    taches_employe.append(
                         {
                              "idProjet" : idProjet,
                              "idTache" : idTache,
                              "idEmploye" : idEmploye,
                              "jour" : v[i][0],
                              "heureDebut" : v[i][j*5 + 2],
                              "heureFin" : v[i][j*5 + 3],
                              "description" : description,
                              "commentaire" : commentaire,
                              "montantFacturable" : float(v[i][j*5 + 4])
                         }
                    )
                    idTache = idTache+1
     curseur.close()
     return

def version1(data):
     """
     Lis les fichiers de format 1.
     Récupère les données à insérer
     """
     global tache, tache_employe
     for k,v in data.items():
          nom = v[0][1]
          prenom = v[1][1]
          try:
               responsable = v[3][1]
          except :
               responsable = ""
          #Récupération de l'id de l'employé avec son nom et prénom
          curseur = bdd.cursor()
          sql = "SELECT id, prenom, nom FROM employe WHERE prenom=%s AND nom=%s"
          val = (prenom, nom)
          curseur.execute(sql, val)
          
          row = curseur.fetchone()


          if row == None: #si l'employé n'est pas dans la table
               print("ERREUR : ")
               print("L'employé '"+prenom+" "+nom+"' n'existe pas dans la table employé.")
               quit()

          
          idEmploye = row[0] #on récupère l'id de l'employé

          curseur.execute("SELECT id FROM tache WHERE id=(SELECT max(id) FROM tache)")
          
          row = curseur.fetchone()
          if row == None:
               idTache = 1
          else : idTache = row[0] +1 #on récupère l'id de la dernière tache
        

          for i in range(6, len(v)):
               nomProjet = v[i][5]
               try : # on récupère l'id du projet
                    sql = "SELECT id FROM projet WHERE nom=%s;"
                    val = (nomProjet,)
                    curseur.execute(sql, val)
                    row = curseur.fetchone()
                    idProjet = row[0]

               except : 
                    idProjet = None
                    print("WARNING : Le projet '"+nomProjet+"' n'existe pas dans la table projet, pas d'insertion.")

               try : description = v[i][6]
               except : description = None
               try: commentaire = v[i][7]
               except : commentaire = None
               taches.append(
                    {
                         "idProjet" : idProjet,
                         "coutEstime" : float(v[i][4]),
                         "dureeEstime" : calculDuree(v[i][2], v[i][3])
                    }
               )
               taches_employe.append(
                    {
                         "idProjet" : idProjet,
                         "idTache" : idTache,
                         "idEmploye" : idEmploye,
                         "jour" : v[i][0],
                         "heureDebut" : v[i][2],
                         "heureFin" : v[i][3],
                         "description" : description,
                         "commentaire" : commentaire,
                         "montantFacturable" : float(v[i][4])
                    }
               )
               idTache = idTache+1
     curseur.close()
     return
     

if __name__=="__main__":

     fichier = "testfile.ods" if len(sys.argv) < 2 else sys.argv[1]
     data = get_data(fichier)

     for _,v in data.items():
          if v[0][0] == "Prénom" :
               print("Document de format 2")
               version2(data)
          else :
               print("Document de format 1")
               version1(data)
          break
     curseur = bdd.cursor()
    
     nb_taches = 0
     nb_tachesEmploye = 0
     #Insertion dans la BDD 
     for une_tache in taches : # table tache
          if (une_tache['idProjet'] != None):
               sql = "INSERT INTO tache (idProjet, coutEstime, dureeEstime) VALUES (%s,%s,%s)"
               val = (une_tache['idProjet'], une_tache['coutEstime'], une_tache['dureeEstime'])
               curseur.execute(sql, val)
               nb_taches = nb_taches+1
     bdd.commit()

     for une_tache in taches_employe : # table tacheEmploye
          if (une_tache['idProjet'] != None):
               sql = "INSERT INTO tacheEmploye (idTache, idEmploye, jour, heureDebut, heureFin, description, commentaire, montantFacturable) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)"
               val = (une_tache['idTache'], une_tache['idEmploye'], une_tache['jour'], une_tache['heureDebut'], une_tache['heureFin'], une_tache['description'], une_tache['commentaire'], une_tache['montantFacturable'])
               curseur.execute(sql, val)
               nb_tachesEmploye = nb_tachesEmploye+1
     bdd.commit()

     print()
     print("table tache : ", nb_taches, "lignes ajoutées")
     print("table tacheEmploye : ", nb_tachesEmploye, "lignes ajoutées")

     bdd.close()
     