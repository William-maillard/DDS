# DDS

Projet DSC1, UE documents et données structurés

## Lancer le projet

  >node server/launch

Cette commande permet de démarer le serveur GET/POST pour avoir accès à l'application client,
et le serveur websocket pour la communication entre application client et server.


NB: utiliser nodemon au lieu de node pour que le serveur se relance automatiquement
lorsque l'on modifie un de ses fichiers.

## Introduction - description du projet

L’objectif du projet est d’extraire des données des feuilles de temps d’une entreprise. Ces documents sont reçus par l’application au format .ods (Open Document Spreadsheet).
Les données extraites seront stockées dans une base de données, dont le schéma entités-association est fourni en annexe1, afin de pouvoir répondre à des requêtes provenant d’une application métier.
L’application permettra de demander des informations sur les employés et les projets en cours. Ces demandes seront paramétrables. Chaque paramètre aura une valeur par défaut, les rendant par conséquent tous facultatifs.

## Informations du document

### Données métiers

+ Les informations sur les employés
  + Nom, prénom
  + Informations de contact (numéro de téléphone, mail)
  + Poste
  + Horaires
  + Projet(s) en cours
  + Employeur (si l’entreprise fait appel à des consultant externe)
  + Coût horaire
+ Les informations sur le projet en cours
  + Estimation en temps
  + Nombre de personnes assignées par semaine
  + Type de poste requis pour effectuer le travail (ex : dev, testeur, chef de projet, …)
  + Coûts (courant, estimé)
  + Les informations sur les clients

### Données à stocker

+ Affectation aux projets des employés
+ Avancement dans la semaine
+ Comptabiliser les heures supp
+ Tâches de projets terminés/à faire/en cours

### Extractration des données

Pour extraire les données nous nous servirons de mots clés contenus dans les en-têtes du tableur. Voici des exemples de mots-clés qui pourront être utilisés : le nom des jours de la semaine, année, mois, date, début, fin, pause, responsable, employé, horaires, congés, tâche, projet.

### Informations consultables

Notre application permettra de questionner la base de données afin de récupérer :

+ Les informations sur un projet
  + Coût en cours total (paramètre : durée)
  + Nombre de personnes qu’il mobilise (paramètre : durée)
  + Nombre d’heure passé dessus (paramètre : durée)
  + Taux d’avancement (paramètre : durée)
  + Paramètres : obtenir les projets d’un client particulier
  + Paramètres : obtenir la somme des points précédents pour tous les projets d’un client
  + Paramètre : nombre de taches restante pour le projet
  + Paramètre : liste des taches du projet, distinguer fait/non fait
+ Les informations sur un employé
  + Son nombre d’heure (paramètre : durée ; défaut semaine)
  + Les heures supp qui lui son dû (paramètre : durée ; défaut mois)
  + Le nombre de tâche qu’il a terminé (paramètre : par projet, en général ; défaut : en général) (paramètre : durée ; défaut semaine)
  + Le nombre de projet sur lequel il collabore (paramètre : durée ; défaut semaine)
  + Ces congés (paramètre : durée ; défaut semaine)
  + Paramètre : sélectionner plusieurs employés
  + Paramètre : sélectionner les employés par entreprise ou manager
  + Paramètre : efficacité de l’employé (= ratio temps pris pour une tache par rapport à son temps estimé)

## Liste des tâches à réaliser

### extraction de données

+ Générer des feuilles de temps
+ Programme d’extraction de données

### Application métier

+ Réalisation de 2 DTD pour chaque type de message (question/réponse)
+ Réalisation d’une application pour créer des messages xml respectant la DTD concernant le message

### Interface applicative avec la base de données

+ Réaliser un programme pour transformer les messages xml en requêtes pour la base de données

## Implémentations

Un site web basique avec deux formulaire (un pour chaque type de message) constituera l’application métier. La création et l’affichage de message xml sera réalisé en javascript.
Nous envisageons aussi d’utiliser javascript (plus spécifiquement nodeJS) pour lire les fichiers ods et en extraire les données.
Pour l’interaction avec la base de données nous choisirons entre JAVA-JPA-SQL et nodeJS-MySQL.

## Annexe 1

![diagramme](docs/diagrammeBDD.png)