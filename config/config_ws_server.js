/**
 * This files contains globals variables for
 * the configuration of the websocket server.
 */
export const WS_SERVER_IP = 'localhost'
export const WS_SERVER_PORT = '11111'