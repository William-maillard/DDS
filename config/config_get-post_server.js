/**
 * This files contains globals variables for
 * the configuration of the get/post server.
 */
 export const SERVER_IP = 'localhost'
 export const SERVER_PORT = process.env.PORT || '11111'
 export const PATH_TEMPLATES = "./client/templates/"