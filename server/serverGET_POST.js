
import express from "express"
//const express = require("express")
import { createServer } from "http"

import { SERVER_IP, SERVER_PORT, PATH_TEMPLATES } from "../config/config_get-post_server.js"

import path from "path"
import serveIndex from "serve-index"
import { Http2ServerRequest } from "http2"


export async function launch_express_server() {
    var app = express()

    // make accessible folder containings ressources to pass to the client
    app.use('/client', express.static(path.resolve('client')), serveIndex(path.resolve('client')))
    app.use('/config', express.static(path.resolve('config')), serveIndex(path.resolve('config')))
    app.use('/work_data_generator', express.static(path.resolve('work_data_generator')), serveIndex(path.resolve('work_data_generator')))
    app.use('/dtd', express.static(path.resolve('DTD')), serveIndex(path.resolve('DTD')))

    app.get("/", function(request, response) {
        //response.send(path.resolve('./client/')) 
        //response.sendFile("index.htm",  { root: PATH_TEMPLATES })
        response.sendFile("formulaire.html",  { root: PATH_TEMPLATES })
    })

    app.get("/generateur", function(request, response) {
        response.sendFile("generator.html", { root: PATH_TEMPLATES })
    })

    app.get("/favicon.ico", function(request, response) {
        console.log("Get favicon")
        response.sendFile("favicon.ico",  { root: "./client/img/" })
    })

    /*app.listen(SERVER_PORT, function(){
        console.log("Started application on port %d", SERVER_PORT)
    })*/

    
    var server = createServer(app)

    server.listen(SERVER_PORT, function(){
        console.log("Started application on port %d", SERVER_PORT)
    })

    return server
}