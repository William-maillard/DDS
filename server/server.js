/**
 * This module contains the Websocket server of the application.
 * To config its port see /config/config_ws_server
 * 
 * This files contain all methods that allow the server to treat
 * and respond to an incomming message from the client.
 * 
 * A client is identify by the WebSocket instance created at his connection.
 * This object is pass trhought all function that are involved in the
 * message treatment and response process.
 */

import { WebSocketServer } from "ws"

import { readFileSync } from "fs"
import path from "path"


import {parseXmlString} from "libxmljs"
import {DOMParser} from "xmldom"
import XPathSelect from "xpath"
import { xsltProcess, xmlParse } from "xslt-processor"

import { connectionDB, executeStatement} from "./db/connection_db2.js"
import { SERVER_IP, SERVER_PORT } from "../config/config_get-post_server.js"
import { WS_SERVER_PORT } from "../config/config_ws_server.js" 



// Creating a new websocket server
export function launch_websocket_server(http_server) {
    /** create a web socket server accessible at the same IP that the http server */
    const wss = new WebSocketServer({ server: http_server })

    // Creating connection using websocket
    wss.on("connection", ws => {
        console.log("A new client is connected.");
    
        /** Event handler for an incoming message **/
        ws.on("message", (data) => {
            console.log(`Client has sent us: ${data}.`)

            /* -- init our variables -- */
            var jsonMessage = JSON.parse(data)
            var xmlMessage

            /** clean the xml string */
            if(jsonMessage.message != undefined){
                jsonMessage.message = jsonMessage.message.replaceAll("\n", "")
            }
            
            /** which type of message do we have ? */
            if(jsonMessage.type == "projet") {
                console.log("type: project")
                /* -- extract and transform the xml string message into an XMLDocument -- */
                xmlMessage = new DOMParser().parseFromString(jsonMessage.message, "text/xml")
                /* -- hand over, to query the DB and construct the answer -- */
                handleMessageProject(ws, xmlMessage)
            }
            else if(jsonMessage.type == "employe") {
                console.log("type: employe")
                /* -- extract and transform the xml string message into an XMLDocument -- */
                xmlMessage = new DOMParser().parseFromString(jsonMessage.message, "text/xml")
                /* -- hand over, to query the DB and construct the answer -- */
                handleMessageEmploye(ws, xmlMessage)
            }
            else{
                console.log("type: unknow") 
            }
        });

        /** what to do when a client disconnects from the server **/
        ws.on("close", () => {
            console.log("A client disconnects from the server.");
        });

        /** handling client connection error **/
        ws.onerror = function (e) {
            if(e.wasClean) {
                console.log('The client socket has been closed '+e.code+', '+e.reason);
            }else {
                console.log('The connexion has been interrupted. :( '+e.code+', '+e.reason);
            }

        }
    })
    console.log("The WebSocket server is running on port %d", WS_SERVER_PORT);
}


/**
 * This Function prepare the query and call the DB method to execute it.
 * @param {WebSocket} ws : socket to identify and communicate with the client
 * @param {XMLDocument} message : the client message received
 */
function handleMessageEmploye(ws, message) {
    // 1.Extract informations from the XMLDocument with xpath
    let nodes = XPathSelect.select("//employe/nom", message)
    var name = nodes[0].firstChild.data

    // 2.Create the SELECT query
    //prenom, nom, telephone, mail, adresse, heuresSup, congePayeRestant
    var query = "SELECT * FROM employe WHERE nom=:name"
    var params = {name: name}

    // 3.Hand over to the DB and give a callback function to execute after the statment
    executeStatement(ws, query, params, buildResponseMessageEmploye)
}

/**
*  Build an xml message to send to the client.
*  It can handle multiples employes response.
* @param {WebSocket} ws : socket to communicate with the client
* @param {RowDataPacket[]} results : array of RowDataPacket object (one attribute for each colomn)
* @param {FieldPacket[]} metadata : array of FieldPacket object containing metadata about each column
*/
function buildResponseMessageEmploye(ws, results, metadata) {
    //1. Get the path of the DTD of the xml to build
    let pathDTD = SERVER_IP+':'+SERVER_PORT+'/dtd/responseEmploye.dtd'

    //2. Construct the xml string of the response
    var message = "<?xml version=\"1.0\"?>\n"
    message += '<!DOCTYPE employe PUBLIC "'+pathDTD+'">\n'

    message += '<liste-employe>'+'\n'
    results.forEach(result => {
        console.log("for each: ", result)
        message += '<employe>'+'\n'
            message += '<nom>'+result.nom+'</nom>'+'\n'
            message += '<prenom>'+result.prenom+'</prenom>'+'\n'
            message += '<tel>'+result.telephone+'</tel>'+'\n'
            message += '<mail>'+result.mail+'</mail>'+'\n'
            message += '<adresse>'+result.adresse+'</adresse>'+'\n'
            //message += '<entreprise>'+'</entreprise>' // make a join for that
            //message += '<responsable>'+'</responsable>' // idem
            message += '<heuresParSemaine>'+result.heuresParSemaine+'</heuresParSemaine>'+'\n'
            message += '<congePayesRestant>'+result.congePayeRestant+'</congePayesRestant>'+'\n'
            message += '<heuresSupp>'+result.heureSup+'</heuresSupp>'+'\n'
        message += '</employe>'+'\n' 
    });
    message += '</liste-employe>'+'\n'
    console.log("Answer message:\n", message)

    //3. Create and send the JSON message through the websocket
    // I just let this part for seeing the xwml message on the client side.
    let data = {
        type: "xmlString",
        message: message
    }
    ws.send(JSON.stringify(data))

    //4. here send message transform to html with an xslt sheet
    /* -- load the xslt sheet into a string -- */
    let xsltString = fileToSTring(path.resolve("./DTD/employe_min.xslt"))


    /* -- Transform the xml file into htm using an xslt -- */
    const outXmlString = xsltProcess( //output xml string. (in html format)
        xmlParse(message),           // string of xml file contents
        xmlParse(xsltString)         // string of xslt file contents
    ).replaceAll("\n", "").replaceAll("\r", "");
    console.log("Message xml in html string: ", outXmlString)

    /* -- Create and send the message -- */
    let data2 = {
        type: "htmlString",
        message: outXmlString,
    }
    ws.send(JSON.stringify(data2))
}

/** message is of type Document */
function handleMessageProject(ws, message) {
    // 1.Extract informations from the XMLDocument with xpath
    let nodes = XPathSelect.select("//projet/nom-projet", message)
    var name = nodes[0].firstChild.data

    // 2.Create the SELECT query
    console.log("nom projet", name)
    var query = `
    SELECT * FROM projet WHERE nom= :name`
    // nom, debut, finEstimee, coutEstime, nbPersonnesRequises
    var params = {name: name}

    // 3.Hand over to the DB and give a callback function to execute after the statment
    executeStatement(ws, query, params, buildResponseMessageProjet)
}

function buildResponseMessageProjet(ws, results, metadata) {
    console.log("response project: ", results)
    //1. Get the path of the DTD of the xml to build
    let pathDTD = SERVER_IP+':'+SERVER_PORT+'/dtd/responseProjet.dtd'

    //2. Construct the xml string of the response
    var message = "<?xml version=\"1.0\"?>\n"
    message += '<!DOCTYPE employe PUBLIC "'+pathDTD+'">\n'

    message += '<liste-projets>'+'\n'
    results.forEach(result => {
        console.log("for each: ", result)
        message += '<projet>'+'\n'
            message += '<nom>'+result.nom+'</nom>'+'\n'
            //message += '<client>'+result.prenom+'</client>'+'\n' // need join
            //message += '<secteur>'+result.telephone+'</secteur>'+'\n'
            message += '<cout>'+result.coutEstime+'</cout>'+'\n'
            message += '<nombre-personnel>'+result.nbPersonnesRequises+'</nombre-personnel>'+'\n'
            //message += '<totalheure>'+result.heuresParSemaine+'</totalheure>'+'\n'
           // message += '<taux-avancement>'+result.congePayeRestant+'</taux-avancement>'+'\n'
           // message += '<liste-taches>'+result.heureSup+'</liste-taches>'+'\n'
        message += '</projet>'+'\n' 
    });
    message += '</liste-projets>'+'\n'
    console.log("Answer message:\n", message)

    //3. Create and send the JSON message through the websocket
    // I just let this part for seeing the xwml message on the client side.
    let data = {
        type: "xmlString",
        message: message
    }
    ws.send(JSON.stringify(data))

    //4. here send message transform to html with an xslt sheet
    /* -- load the xslt sheet into a string -- */
    let xsltString = fileToSTring(path.resolve("./DTD/projet_min.xslt"))


    /* -- Transform the xml file into htm using an xslt -- */
    const outXmlString = xsltProcess( //output xml string. (in html format)
        xmlParse(message),           // string of xml file contents
        xmlParse(xsltString)         // string of xslt file contents
    ).replaceAll("\n", "").replaceAll("\r", "");
    console.log("Message xml in html string: ", outXmlString)

    /* -- Create and send the message -- */
    let data2 = {
        type: 'htmlString-projet',
        message: outXmlString,
    }
    ws.send(JSON.stringify(data2))
}


/**
 * Read a file and put its content in a string 
 * @param {String} filePath : an absolute or relative path of a file
 * @returns the content of the file in a String
 */
function fileToSTring(filePath) {
    return readFileSync(filePath, "utf8")
}