/**
 * This file contains function to connect the app
 * with the database given user and pw in the 
 * config_db.js file.
 */
import {DBNAME, HOST, USER, PWD} from "../../../config/config_db.js"
import { createConnection } from "mysql"

console.log('Get connection:')

export var connectionDB = createConnection({
    database:DBNAME,
    host:HOST,
    user:USER,
    password:PWD
})

connectionDB.connect(function(error){
    if(error){
        console.log('DB connection failed.')
        throw error
    }
    console.log('Connexion established.')
})