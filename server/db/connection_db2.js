/**
 * This file contains function to connect the app
 * with the database given user and pw in the 
 * config_db.js file.
 * This file use the mysql2 module
 */
 import {DBNAME, HOST, USER, PWD} from "../../config/config_db.js"

import Sequelize from "sequelize"
 

console.log('Get connection to the Data Base:')
export const connectionDB = new Sequelize(
    DBNAME, 
    USER, 
    PWD,
     {    
        dialect: "mysql",    
        host: HOST
    }
)

/* -- connection test -- */
try {   
    connectionDB.authenticate();   
    console.log('Data Base connexion established.')
 } catch (error) {  
     console.error('DB connection failed with error: ', error)
}

/* -- execute a prepared statement -- */

/**
 * 
 * @param {WebSocket} ws : socket to communicate with the client
 * @param {String} query : string representing an SQL prepared statement, parameter are precede by :
 * @param {JSONObject} objectParams : key name of the variable in the query (:name), value the replaced one in the query
 * @param {Function} callback : a function to call after the query has been executed, the result and metadata are given as parameters
 */
export async function executeStatement(ws, query, objectParams, callback) {
    await connectionDB.query(
        query,
        {replacements : objectParams }
    ).then(([results, metadata]) => {
        /**
        *  Function which exexute the above query.
        * @param {RowDataPacket[]} results : array of RowDataPacket object (one attribute for each colomn)
        * @param {FieldPacket[]} metadata : array of FieldPacket object containing metadata about each column
        */
        console.log("Query executed, calling the callback function:", results, metadata)
        callback(ws, results, metadata)
    })
}


export function requestProjectByName(name) {
    connectionDB.query(

         [name],
         function(error, results) {
            if(error) {
                console.log("An error happen for the query of an employe named : "+name)
            }
            return results
         }
    )
}