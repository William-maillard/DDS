-- MySQL dump 10.13  Distrib 8.0.31, for Linux (x86_64)
--
-- Host: localhost    Database: dds
-- ------------------------------------------------------
-- Server version	8.0.31-0ubuntu0.22.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employe`
--

DROP TABLE IF EXISTS `employe`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `employe` (
  `id` int NOT NULL AUTO_INCREMENT,
  `prenom` varchar(50) DEFAULT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `idEntreprise` int DEFAULT NULL,
  `idResponsableInterne` int DEFAULT NULL,
  `heuresParSemaine` int DEFAULT NULL,
  `congePayeRestant` int DEFAULT NULL,
  `heuresSup` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idEntreprise` (`idEntreprise`),
  KEY `idResponsableInterne` (`idResponsableInterne`),
  CONSTRAINT `employe_ibfk_1` FOREIGN KEY (`idEntreprise`) REFERENCES `entreprise` (`id`),
  CONSTRAINT `employe_ibfk_2` FOREIGN KEY (`idResponsableInterne`) REFERENCES `employe` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employe`
--

LOCK TABLES `employe` WRITE;
/*!40000 ALTER TABLE `employe` DISABLE KEYS */;
INSERT INTO `employe` VALUES (1,'prenom emp 1','nom emp 1','01 01 01 11 01','n1@employe.fr','adresse emp1',1,NULL,1,1,1),(2,'prenom emp 2','nom emp 2','02 02 02 22 02','n2@employe.fr','adresse emp2',1,1,2,2,2),(3,'prenom emp 3','nom emp 3','03 03 03 33 03','n3@employe.fr','adresse emp3',1,1,3,3,3),(4,'prenom emp 4','nom emp 4','04 04 04 44 04','n4@employe.fr','adresse emp4',2,NULL,4,4,4),(5,'prenom emp 5','nom emp 5','05 05 05 55 05','n5@employe.fr','adresse emp5',2,4,5,5,5),(6,'Eldridge','Kunde','501-245-108','Eldridge70@hotmail.com','2904 Hilario Islands',NULL,NULL,36,26,24),(7,'Arden','O\'Conner','501-305-300','Arden53@gmail.com','417 Feest Viaduct',NULL,NULL,36,48,18),(8,'Ollie','Farrell','501-953-039','Ollie4@yahoo.com','19553 Pietro Plaza',NULL,NULL,36,11,15),(9,'Eveline','Kreiger','501-263-694','Eveline.Schneider@gmail.com','16564 Morar Drive',NULL,NULL,36,11,26),(10,'Javonte','Lang','501-893-079','Javonte.Dare@gmail.com','213 Crist Mills',NULL,NULL,36,36,10),(11,'Irma','Corwin','501-582-113','Irma2@yahoo.com','20827 Caesar Extension',NULL,NULL,36,15,24),(12,'Dora','Kling','501-756-897','Dora_Murray@gmail.com','22600 Metz Extensions',NULL,NULL,36,39,19),(13,'Raul','Harber','501-159-047','Raul18@yahoo.com','863 Isac Walks',NULL,NULL,36,28,27),(14,'Curtis','Terry','501-519-087','Curtis.Wisoky@hotmail.com','98277 Hagenes Divide',NULL,NULL,36,14,5),(15,'Delia','Heaney','501-444-441','Delia.Gusikowski33@yahoo.com','7037 Cremin Route',NULL,NULL,36,50,26);
/*!40000 ALTER TABLE `employe` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entreprise` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) DEFAULT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entreprise`
--

LOCK TABLES `entreprise` WRITE;
/*!40000 ALTER TABLE `entreprise` DISABLE KEYS */;
INSERT INTO `entreprise` VALUES (1,'entreprise n1','01 11 11 11 11','entreprise1@bidule.com','adresse n1'),(2,'entreprise n2','02 22 22 22 22','entreprise2@bidule.com','adresse n2'),(3,'entreprise n3','03 33 33 33 33','entreprise3@bidule.com','adresse n3'),(4,'Ritchie - Mayer','501-564-591','Ritchie-Mayer34@yahoo.com','Maricopa'),(5,'Ziemann, Runolfsson and Muller','501-069-892','ZiemannRunolfssonandMuller_Pouros@yahoo.com','Detroit'),(6,'Pacocha, Hudson and Murray','501-170-364','PacochaHudsonandMurray70@gmail.com','Biloxi'),(7,'Stokes - Tromp','501-731-290','Stokes-Tromp_Conn@gmail.com','Nashville-Davidson'),(8,'Kris - Mayert','501-117-989','Kris-Mayert77@hotmail.com','Buena Park'),(9,'Padberg - Hagenes','501-058-158','Padberg-Hagenes.Jast@yahoo.com','Richland'),(10,'Murazik, Trantow and Carroll','501-946-531','MurazikTrantowandCarroll.Smith40@hotmail.com','Port Orange'),(11,'Pacocha - Beatty','501-557-603','Pacocha-Beatty.Schulist32@gmail.com','Encinitas'),(12,'Beer, Zboncak and Ortiz','501-106-931','BeerZboncakandOrtiz75@gmail.com','Niagara Falls'),(13,'Bartell, Daugherty and Leannon','501-866-783','BartellDaughertyandLeannon_Fay57@yahoo.com','High Point');
/*!40000 ALTER TABLE `entreprise` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projet`
--

DROP TABLE IF EXISTS `projet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `projet` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idClient` int DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `debut` date DEFAULT NULL,
  `finEstimee` date DEFAULT NULL,
  `coutEstime` float DEFAULT NULL,
  `nbPersonnesRequises` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idClient` (`idClient`),
  CONSTRAINT `projet_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `entreprise` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projet`
--

LOCK TABLES `projet` WRITE;
/*!40000 ALTER TABLE `projet` DISABLE KEYS */;
INSERT INTO `projet` VALUES (1,2,'projet n1','1111-11-01','2111-11-01',1111.1,112),(2,2,'projet n2','2222-12-02','3222-12-02',2222.2,223),(3,3,'projet n3','3333-03-03','3433-03-03',33.3,33),(4,NULL,'challenge','2022-10-23','2022-10-06',NULL,91),(5,NULL,'orchestration','2022-11-16','2022-11-02',NULL,87),(6,NULL,'benchmark','2022-11-05','2022-11-02',NULL,86),(7,NULL,'protocol','2022-10-17','2022-10-01',NULL,99),(8,NULL,'hardware','2022-10-09','2022-09-28',NULL,66),(9,NULL,'alliance','2022-11-10','2022-10-26',NULL,53),(10,NULL,'support','2022-11-22','2022-11-07',NULL,35),(11,NULL,'local area network','2022-10-24','2022-10-20',NULL,41),(12,NULL,'emulation','2022-10-16','2022-09-29',NULL,22),(13,NULL,'strategy','2022-11-15','2022-11-14',NULL,105);
/*!40000 ALTER TABLE `projet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tache`
--

DROP TABLE IF EXISTS `tache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tache` (
  `id` int NOT NULL AUTO_INCREMENT,
  `idProjet` int DEFAULT NULL,
  `coutEstime` float DEFAULT NULL,
  `dureeEstime` time DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idProjet` (`idProjet`),
  CONSTRAINT `tache_ibfk_1` FOREIGN KEY (`idProjet`) REFERENCES `projet` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tache`
--

LOCK TABLES `tache` WRITE;
/*!40000 ALTER TABLE `tache` DISABLE KEYS */;
INSERT INTO `tache` VALUES (1,1,1,'10:10:10'),(2,1,2,'20:20:20'),(3,2,3,'30:30:30'),(4,2,4,'40:40:40'),(5,3,5,'50:50:50');
/*!40000 ALTER TABLE `tache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tacheEmploye`
--

DROP TABLE IF EXISTS `tacheEmploye`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tacheEmploye` (
  `idTache` int DEFAULT NULL,
  `idEmploye` int DEFAULT NULL,
  `jour` date DEFAULT NULL,
  `heureDebut` time DEFAULT NULL,
  `heureFin` time DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `montantFacturable` float DEFAULT NULL,
  KEY `idTache` (`idTache`),
  KEY `idEmploye` (`idEmploye`),
  CONSTRAINT `tacheemploye_ibfk_1` FOREIGN KEY (`idTache`) REFERENCES `tache` (`id`),
  CONSTRAINT `tacheemploye_ibfk_2` FOREIGN KEY (`idEmploye`) REFERENCES `employe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tacheEmploye`
--

LOCK TABLES `tacheEmploye` WRITE;
/*!40000 ALTER TABLE `tacheEmploye` DISABLE KEYS */;
INSERT INTO `tacheEmploye` VALUES (1,1,'1111-11-10','11:11:00','21:11:01','description 1','commentaire 1',11.11),(2,2,'2222-12-22','22:22:02','42:22:02','description 2','commentaire 2',22.22),(3,2,'3333-03-13','33:33:03','63:33:03','description 3','commentaire 3',33.33),(4,2,'4444-04-24','44:24:04','44:44:44','description 4','commentaire 4',44.44),(5,1,'5555-05-15','55:25:05','56:55:55','description 5','commentaire 5',55.55);
/*!40000 ALTER TABLE `tacheEmploye` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-23 13:18:18
