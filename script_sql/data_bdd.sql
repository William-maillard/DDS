/* ------ On vide et reset les tables ------ */
SET FOREIGN_KEY_CHECKS=0;
DELETE FROM tacheEmploye;
DELETE FROM tache;
DELETE FROM employe;
DELETE FROM projet;
DELETE FROM entreprise;

ALTER TABLE tacheEmploye AUTO_INCREMENT = 1;
ALTER TABLE tache AUTO_INCREMENT = 1;
ALTER TABLE employe AUTO_INCREMENT = 1;
ALTER TABLE projet AUTO_INCREMENT = 1;
ALTER TABLE entreprise AUTO_INCREMENT = 1;
SET FOREIGN_KEY_CHECKS=1;
/* -------------------------------- */


INSERT INTO entreprise (nom, telephone, mail, adresse) 
VALUES ("entreprise n1","01 11 11 11 11","entreprise1@bidule.com","adresse n1");

INSERT INTO entreprise (nom, telephone, mail, adresse) 
VALUES ("entreprise n2","02 22 22 22 22","entreprise2@bidule.com","adresse n2");

INSERT INTO entreprise (nom, telephone, mail, adresse) 
VALUES ("entreprise n3","03 33 33 33 33","entreprise3@bidule.com","adresse n3");



INSERT INTO projet (idClient, nom, debut, finEstimee, coutEstime, nbPersonnesRequises) 
VALUES (2,"projet n1","1111-11-01","2111-11-01", 1111.1, 112);

INSERT INTO projet (idClient, nom, debut, finEstimee, coutEstime, nbPersonnesRequises) 
VALUES (2,"projet n2","2222-12-02","3222-12-02", 2222.2, 223);

INSERT INTO projet (idClient, nom, debut, finEstimee, coutEstime, nbPersonnesRequises) 
VALUES (3,"projet n3","3333-03-03","3433-03-03", 33.3, 33);



INSERT INTO employe (prenom, nom, telephone, mail, adresse, idEntreprise, idResponsableInterne, heuresParSemaine, congePayeRestant, heuresSup) 
VALUES ("prenom emp 1","nom emp 1","01 01 01 11 01","n1@employe.fr", "adresse emp1", 1, NULL, 1, 1, 1);

INSERT INTO employe (prenom, nom, telephone, mail, adresse, idEntreprise, idResponsableInterne, heuresParSemaine, congePayeRestant, heuresSup) 
VALUES ("prenom emp 2","nom emp 2","02 02 02 22 02","n2@employe.fr", "adresse emp2", 1, 1, 2, 2, 2);

INSERT INTO employe (prenom, nom, telephone, mail, adresse, idEntreprise, idResponsableInterne, heuresParSemaine, congePayeRestant, heuresSup) 
VALUES ("prenom emp 3","nom emp 3","03 03 03 33 03","n3@employe.fr", "adresse emp3", 1, 1, 3, 3, 3);

INSERT INTO employe (prenom, nom, telephone, mail, adresse, idEntreprise, idResponsableInterne, heuresParSemaine, congePayeRestant, heuresSup) 
VALUES ("prenom emp 4","nom emp 4","04 04 04 44 04","n4@employe.fr", "adresse emp4", 2, NULL, 4, 4, 4);

INSERT INTO employe (prenom, nom, telephone, mail, adresse, idEntreprise, idResponsableInterne, heuresParSemaine, congePayeRestant, heuresSup) 
VALUES ("prenom emp 5","nom emp 5","05 05 05 55 05","n5@employe.fr", "adresse emp5", 2, 4, 5, 5, 5);



INSERT INTO tache (idProjet, coutEstime, dureeEstime)
VALUES (1, 1, "10:10:10");

INSERT INTO tache (idProjet, coutEstime, dureeEstime)
VALUES (1, 2, "20:20:20");

INSERT INTO tache (idProjet, coutEstime, dureeEstime)
VALUES (2, 3, "30:30:30");

INSERT INTO tache (idProjet, coutEstime, dureeEstime)
VALUES (2, 4, "40:40:40");

INSERT INTO tache (idProjet, coutEstime, dureeEstime)
VALUES (3, 5, "50:50:50");




INSERT INTO tacheEmploye (idTache, idEmploye, jour, heureDebut, heureFin, description, commentaire, montantFacturable)
VALUES (1, 1, "1111-11-10", "11:11:00", "21:11:01", "description 1", "commentaire 1", 11.11);

INSERT INTO tacheEmploye (idTache, idEmploye, jour, heureDebut, heureFin, description, commentaire, montantFacturable)
VALUES (2, 2, "2222-12-22", "22:22:02", "42:22:02", "description 2", "commentaire 2", 22.22);

INSERT INTO tacheEmploye (idTache, idEmploye, jour, heureDebut, heureFin, description, commentaire, montantFacturable)
VALUES (3, 2, "3333-03-13", "33:33:03", "63:33:03", "description 3", "commentaire 3", 33.33);

INSERT INTO tacheEmploye (idTache, idEmploye, jour, heureDebut, heureFin, description, commentaire, montantFacturable)
VALUES (4, 2, "4444-04-24", "44:24:04", "44:44:44", "description 4", "commentaire 4", 44.44);

INSERT INTO tacheEmploye (idTache, idEmploye, jour, heureDebut, heureFin, description, commentaire, montantFacturable)
VALUES (5, 1, "5555-05-15", "55:25:05", "56:55:55", "description 5", "commentaire 5", 55.55);



select * from entreprise;
select * from projet;
select * from employe;
select * from tache;
select * from tacheEmploye;