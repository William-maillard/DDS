/**
 * This file gather informations that have been given in the form, 
 * and build a string xml message validating a dtd.
 * 
 * Then it send the message to the server through a websocket,
 * thus the client need to be connected.
 * 
 * We assume that the file global_variables.js has been included in
 * the page before this file.
 * Thus we dispose of all of these variables.
 * */
var message;
var fields = {};

/**
 * Since the variable message is global, we need to reinit it
 * each time we want to send a message without refreshing the page.
 */
function init_message() {
    message = "<?xml version=\"1.0\"?>\n";
    fields = {};
}

function creerMessageEmploye(){
    init_message();

    fields.nomEmploye = document.getElementById('nom-employe').value;
    fields.prenomEmploye = document.getElementById('prenom-employe').value;
    fields.emailEmploye = document.getElementById('email-employe').value;
    fields.telEmploye = document.getElementById('tel-employe').value;
    fields.adresseEmploye = document.getElementById('adresse-employe').value;

    message += '<!DOCTYPE employe PUBLIC "localhost:11111/dtd/requeteEmploye.dtd">\n'

    message += "<employe>\n";

    message += "<nom>"+ JSON.parse(JSON.stringify(fields.nomEmploye))+"</nom>\n";

    if((JSON.parse(JSON.stringify(fields.prenomEmploye)).trim().length)){
        message += "<prenom>" + JSON.parse(JSON.stringify(fields.prenomEmploye))+"</prenom>\n";
    }

    if(JSON.parse(JSON.stringify(fields.emailEmploye)).trim().length){
        message += "<email>" + JSON.parse(JSON.stringify(fields.emailEmploye))+"</email>\n";
    }

    if(JSON.parse(JSON.stringify(fields.telEmploye)).trim().length){
        message += "<tel>" + JSON.parse(JSON.stringify(fields.telEmploye))+"</tel>\n";
    }

    if(JSON.parse(JSON.stringify(fields.adresseEmploye)).trim().length){
        message += "<adresse>" + JSON.parse(JSON.stringify(fields.adresseEmploye))+"</adresse>\n";
    }

    message += "</employe>";
    envoyer("employe");
}



function creerMessageResponsable(){
    init_message();
    
    fields.nomResponsable = document.getElementById('nom-responsable').value;
    fields.prenomResponsable = document.getElementById('prenom-responsable').value;

    message += "<employe>\n";

    message += "<nom-responsable>"+ JSON.parse(JSON.stringify(fields.nomResponsable))+"</nom-responsable>\n";

    if((JSON.parse(JSON.stringify(fields.prenomResponsable)).trim().length)){
        message += "<prenom-responsable>" + JSON.parse(JSON.stringify(fields.prenomResponsable))+"</prenom-responsable>\n";
    }

    message += "</employe>";
    envoyer("responsable");
}





function creerMessageEntreprise(){
    init_message();

    fields.entreprise = document.getElementById('entreprise').value;

    message += "<employe>\n";

    message += "<entreprise>"+ JSON.parse(JSON.stringify(fields.entreprise))+"</entreprise>\n";

    message += "</employe>";
    envoyer("entreprise");
}





function creerMessageProjet(){
    init_message();

    message += "<projet>\n";

    fields.nomProjet = document.getElementById('nom-projet').value;
    fields.nomClient = document.getElementById('nom-client').value;
    fields.secteur = document.getElementById('secteur').value;
    
    message += "<nom-projet>"+ JSON.parse(JSON.stringify(fields.nomProjet))+"</nom-projet>\n";

    if((JSON.parse(JSON.stringify(fields.nomClient)).trim().length)){
        message += "<nom-client>" + JSON.parse(JSON.stringify(fields.nomClient))+"</nom-client>\n";
    }

    if(JSON.parse(JSON.stringify(fields.secteur)).trim().length){
        message += "<secteur>" + JSON.parse(JSON.stringify(fields.secteur))+"</secteur>\n";
    }
    message += "</projet>";

    envoyer("projet");
}



function envoyer(type){
    /* -- create a JSON object to send to the websocket -- */
    let messageXml = JSON.parse(JSON.stringify(message));
    let data = {
        "type" : type,
        "message": messageXml
    };

    console.log(data);
    alert(messageXml);

    ws.send(JSON.stringify(data));
}

