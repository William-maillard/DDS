/**
 * This file contain methods to create xml tag
 * in string format.
 * name and content arguments are strings
 * attributes is an array of [name, value]
 */

function leftTag(name) {
    return '<'+name+'>'
}
function leftTagWithAttributes(name, attributes) {
    var s = '<'+name+

    attributes.forEach(element => {
        s += ' '+attributes[0]+'="'+attributes[1]+'"'
    });

    s += '>'

    return s
}
function rightTag(name) {
    return '</'+name+'>'
}
function tag(name, content) {
    return leftTag(name)+content+rightTag(name)
}
function tagWithAttributes(name, attributes, content) {
    return leftTagWithAttributes(name, attributes)+content+rightTag(name)
}
function singleTag(name) {
    return '<'+name+' />'
}
function singleTagWithAttributes(name, attributes) {
    return leftTagWithAttributes(name, attributes)
}