/**
 * Contains a global websocket variable and
 * functions to create it and manipulate it.
 */

// NB: import the file in the html file which used this file
//import { WS_SERVER_IP, WS_SERVER_PORT } from "../config/config_ws_server"

/** -- create the socket and its events handler -- */
function createWebSocket() {
    ws = new WebSocket("ws://"+WS_SERVER_IP+":"+WS_SERVER_PORT)

    // Chose of the data type received and send by the socket :
 	// socket.binaryType = "blob"; // default value : https://developer.mozilla.org/fr/docs/Web/API/Blob
 	// socket.binaryType = "arraybuffer"; // it's a bites'buffer : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer

    /** -- websocket events handlers -- */
    
    ws.onopen = function(e) {
        console.log("Websocket connexion have been established with "+WS_SERVER_IP+" on the port "+WS_SERVER_PORT)
        /** here som actions to do at the connection */
    }

    ws.onclose = (e) => {
        toggleMessageDisplay()
        // Is the deconnection triggered by the user ?
        if(e.wasClean) {
			console.log('The client socket has been closed '+e.code+', '+e.reason);
		}else {
			console.log('The connexion has been interrupted. :( '+e.code+', '+e.reason);
		}
    }

    ws.onmessage = (e) => {
        // debugg messages
        console.log("A message has been received.")

        /** Treat the message according to its type */
        var jsonMessage = JSON.parse(e.data)
        jsonMessage.message = jsonMessage.message.replaceAll("\n", "")

        if(jsonMessage.type == 'xmlString') {
            console.log("xml string received.")
        }
        else if(jsonMessage.type == 'htmlString'){
            console.log("html string employe received, turn it into HTMLDocument")
            // here parse the xml message received to call a display function
            let html = new DOMParser().parseFromString(jsonMessage.message, "text/html")
            console.log("Html doc:", html)

            let body = html.querySelector("body")
            console.log("Message html body in DOM: ", body)

            let table = html.querySelector("table")
            console.log("Table of employes: ", table)

            // add the result to the page 
            document.querySelector("#response-employe").innerHTML = body.innerHTML
        }
        else if(jsonMessage.type == 'htmlString-projet') {
            console.log("html string projet received, turn it into HTMLDocument")
            
            let html = new DOMParser().parseFromString(jsonMessage.message, "text/html")
            console.log("Html doc:", html)

            let body = html.querySelector("body")
            console.log("Message html body in DOM: ", body)

            let table = html.querySelector("table")
            console.log("Table of projet: ", table)

            
            // add the result to the page 
            document.querySelector("#response-projet").innerHTML = body.innerHTML
        }

    }

    ws.onerror = (e) => {
        console.log("Error, unable to connect to the server.")
        //toggleMessageDisplay()
    }
}


function toggleMessageDisplay()  {
    document.getElementById('div-connect').classList.toggle("hide")
    document.getElementById('div-disconnect').classList.toggle("hide")
}