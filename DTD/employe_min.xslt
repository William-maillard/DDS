<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html> 
<body>
  <h2>Informations trouvées sur l'employé:</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th style="text-align:left">Nom</th>
      <th style="text-align:left">Prenom</th>
      <th style="text-align:left">n°</th>
      <th style="text-align:left">mail</th>
      <th style="text-align:left">adresse</th>
      <th style="text-align:left">h/semaine</th>
      <th style="text-align:left">congés restant</th>
      <th style="text-align:left">heures sup</th>
    </tr>
    <xsl:for-each select="liste-employe/employe">
    <tr>
      <td><xsl:value-of select="nom"/></td>
      <td><xsl:value-of select="prenom"/></td>
      <td><xsl:value-of select="tel"/></td>
      <td><xsl:value-of select="mail"/></td>
      <td><xsl:value-of select="adresse"/></td>
      <td><xsl:value-of select="heuresParSemaine"/></td>
      <td><xsl:value-of select="congePayesRestant"/></td>
      <td><xsl:value-of select="heuresSupp"/></td>
    </tr>
    </xsl:for-each>
  </table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>