<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
<html> 
<body>
  <h2>Informations trouvées le projet:</h2>
  <table border="1">
    <tr bgcolor="#9acd32">
      <th style="text-align:left">Nom</th>
      <th style="text-align:left">Client</th>
      <th style="text-align:left">début</th>
      <th style="text-align:left">finEsimee</th>
      <th style="text-align:left">CoutEstime</th>
      <th style="text-align:left">nbPersonnesRequises</th>
    </tr>
    <xsl:for-each select="liste-projets/projet">
    <tr>
      <td><xsl:value-of select="nom"/></td>
      <td><xsl:value-of select="client"/></td>
      <td><xsl:value-of select="taux-avancement"/></td>
      <td><xsl:value-of select="totalheures"/></td>
      <td><xsl:value-of select="cout"/></td>
      <td><xsl:value-of select="nombre-personnel"/></td>
    </tr>
    </xsl:for-each>
  </table>
</body>
</html>
</xsl:template>
</xsl:stylesheet>