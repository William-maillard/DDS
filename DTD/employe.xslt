<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html><header><link rel="stylesheet" href="/client/css/employe.css"></link></header>
            <body>
                <center>
                    <h1>Reponse Employer</h1>
                </center>
                <xsl:for-each select="liste-employes">
                    <h2>Nombre Message Employe: <xsl:value-of select="count(employe)"/>.</h2>
                    <h2>Nombre Message Responsable: <xsl:value-of select="count(responsable)"/>.</h2>
                    <h2>Nombre Message Entreprise: <xsl:value-of select="count(entreprise)"/>.</h2>
                    
                    
                    
                </xsl:for-each>
                <table border="1" width="100%" class="i">
                    <tr>
                       
                        <th>
                            ID Employe</th>
                        <th>Contrat
                        </th>
                        <th>Salaire
                        </th>
                        
                        
                        <th>
                            Nombre d'heures par semaine</th>
                        <th>
                            Congés payés restants</th>
                        <th>
                            Nombre d'heures supplémentaires</th>
                        <th>
                            Lister les tâches</th>
                        <th>
                            Lister les projets</th>
                     
                    </tr>
                    
                    <xsl:for-each select="liste-employes"><hr></hr>
                        <fieldset class="item header"> 
                            <b> <li> <xsl:value-of select="@message-id"/></li>
                            <hr></hr>
                                <li><xsl:value-of select="@date_message"/></li></b><hr></hr>
                       </fieldset><hr></hr>
                            <xsl:for-each select="employe">
                                   <b> Nom Employer:  <xsl:value-of select="name "/><hr></hr>
                                    Couriel: <xsl:value-of select="mail"/><hr></hr>
                                    Telephone: <xsl:value-of select="tel"/><hr></hr>
                                       Adresse: <xsl:value-of select="adresse"/> <hr></hr></b>
                                
                                <tr> 
                               <td> <xsl:value-of select="@id"/></td>
                                <td> <xsl:value-of select="@contrat"/></td>
                                <td> <xsl:value-of select="@salaire"/></td>
                                
                              
                                
                                <td> <xsl:value-of select="heuresParSemaine"/></td>
                                <td><xsl:value-of select="congePayeRestant"/></td>
                                <td> <xsl:value-of select="heuresSupp"/></td>
                                <td><xsl:value-of select="liste-taches/@nombre"/></td>
                                <td><xsl:value-of select="liste-projets/@nombre"/></td>  </tr>
                            </xsl:for-each>
                        <tr>
                          
                                <td colspan="7"> Employe en poste</td>
                                <td> <xsl:value-of select="count(employe[@contrat='vrai'])"/></td>
                            </tr></xsl:for-each>
                    
                </table><hr></hr><hr></hr><br></br><br></br>
                <center>
                    <h1>Reponse des employés par rapport à leur responsable</h1>
                </center>
                <table border="1" width="100%" class="i">
                    <tr>
                        
                        <th>
                            ID Responsable</th>
                        <th> Non Responsable
                        </th>
                        <th> Adresse Responsable
                        </th>
                        <xsl:for-each select="liste-employes"><hr></hr>
                            <fieldset class="item header"> 
                                <b> <li> <xsl:value-of select="@message-id"/></li>
                                    <hr></hr>
                                    <li><xsl:value-of select="@date_message"/></li></b><hr></hr>
                            </fieldset><hr></hr>
                            <xsl:for-each select="responsable">
                                <tr> <td><xsl:value-of select="@id_responsable"/></td>
                                    <td><xsl:value-of select="name_responsable "/></td>
                                    <td>  <xsl:value-of select="adresse_responsable"/></td></tr>
                                                                
                            </xsl:for-each></xsl:for-each></tr></table>
                       
                <hr></hr><hr></hr><br></br><br></br>
                
                
                <center>
                    <h1>Reponse des employés par rapport à leur entreprise</h1>
                </center>
                <table border="1" width="100%" class="i">
                    <tr>
                        
                        <th>
                            ID Entreprise</th>
                        <th> Non Entreprise
                        </th>
                        <th> Adresse Entreprise
                        </th>
                        <xsl:for-each select="liste-employes"><hr></hr>
                            <fieldset class="item header"> 
                                <b> <li> <xsl:value-of select="@message-id"/></li>
                                    <hr></hr>
                                    <li><xsl:value-of select="@date_message"/></li></b><hr></hr>
                            </fieldset><hr></hr>
                            <xsl:for-each select="entreprise">
                                <tr> <td><xsl:value-of select="@id_entreprise"></xsl:value-of></td>
                                    <td><xsl:value-of select="nom_entreprise"/></td>
                                    <td>  <xsl:value-of select="adresse_entreprise"/></td></tr>
                                
                            </xsl:for-each></xsl:for-each></tr></table>   
                <hr></hr><hr></hr><br></br><br></br>
                
                <center>
                    <h1>Reponse Employer</h1>
                </center>
                
                <table border="1" width="100%" class="i">
                    <tr>
                        
                        <th>
                            ID Employe</th>
                        <th>Contrat
                        </th>
                        <th>Salaire
                        </th>
                        
                        
                        <th>
                            Nombre d'heures par semaine</th>
                        <th>
                            Congés payés restants</th>
                        <th>
                            Nombre d'heures supplémentaires</th>
                        <th>
                            Lister les tâches</th>
                        <th>
                            Lister les projets</th>
                        
                    </tr>
                    
                    <xsl:for-each select="liste-employes"><hr></hr>
                        <fieldset class="item header"> 
                            <b> <li> <xsl:value-of select="@message-id"/></li>
                                <hr></hr>
                                <li><xsl:value-of select="@date_message"/></li></b><hr></hr>
                        </fieldset><hr></hr>
                        <xsl:for-each select="employe">
                            <b> Nom Employer:  <xsl:value-of select="name "/><hr></hr>
                                Couriel: <xsl:value-of select="mail"/><hr></hr>
                                Telephone: <xsl:value-of select="tel"/><hr></hr>
                                Adresse: <xsl:value-of select="adresse"/> <hr></hr></b>
                            
                            <tr> 
                                <td> <xsl:value-of select="@id"/></td>
                                <td> <xsl:value-of select="@contrat"/></td>
                                <td> <xsl:value-of select="@salaire"/></td>
                                
                                
                                
                                <td> <xsl:value-of select="heuresParSemaine"/></td>
                                <td><xsl:value-of select="congePayeRestant"/></td>
                                <td> <xsl:value-of select="heuresSupp"/></td>
                                <td><xsl:value-of select="liste-taches/@nombre"/></td>
                                <td><xsl:value-of select="liste-projets/@nombre"/></td>  </tr>
                        </xsl:for-each>
                        <tr>
                            
                            <td colspan="7"> Employe en poste</td>
                            <td> <xsl:value-of select="count(employe[@contrat='vrai'])"/></td>
                        </tr></xsl:for-each>
                    
                </table><hr></hr><hr></hr><br></br><br></br>
               
                
                
                <style>
                    h1 {
                    color: #1c87c9;
                    }
                    h2 {
                    color: green;
                    }
                    
                    p {
                    color: #8ebf42;
                    }
                </style>
                
                      
                                            
                                                    
          
                
               
                     
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>