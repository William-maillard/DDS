<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="/">
        <html>
            <header><link rel="stylesheet" href="/client/css/projet.css"></link></header>
            <body>
                <center>
                    <h1>Reponse Projet</h1>
                </center>
                <xsl:for-each select="listmessage">
                    <h2>Nombre Message: <xsl:value-of select="count(message)"/>.</h2>
                    
                </xsl:for-each>
               
                <xsl:for-each select="listmessage/message"><hr></hr>
                    <fieldset class="item header"> <li>
                      
                        <b><xsl:value-of select="@message-id"/> </b></li>
                   
                    <li><b> <xsl:value-of select="date_message"/></b>
                                              
                    </li> </fieldset><hr></hr>
                    <table border="1" width="90%" class="i">
                        <tr>
                            <th>
                                ID projet</th>
                            <th>
                                Code projet
                            </th>
                            <th>
                                Date Debut projet
                            </th>
                            <th>
                                Date Fin projet
                            </th>
                            <th>
                                Etat projet
                            </th>
                            <th>
                                Cout Estimer
                            </th>
                            <th>
                                Cout Encour
                            </th>
                            <th>
                                nombre total d'heures
                            </th>
                            <th>
                                Taux Avancement
                            </th>
                            <th>
                                Lister les tâches
                            </th>
                            <th>
                                Lister Des Employer
                            </th>
                        </tr>
                    <xsl:for-each select="requestProjetmembre">
                        <xsl:for-each select="projet"><hr></hr>
                            <b>Nom Projet: <xsl:value-of select="nom"/><hr></hr>
                            Nom Client: <xsl:value-of select="client"/><hr></hr>
                                Secteur:<xsl:value-of select="secteur"/> </b><hr></hr>
                            
                            <tr>
                                <td><xsl:value-of select="@id_projet"/></td>
                                <td><xsl:value-of select="@code"/></td>
                                <td><xsl:value-of select="@datedebut"/></td>
                                <td><xsl:value-of select="@datefin"/></td>
                                <td><xsl:value-of select="@Etat"/></td>
                                <xsl:for-each select="cout">
                                    <td><xsl:value-of select="@estime"/></td> 
                                    <td><xsl:value-of select="@en-cours"/></td> 
                                </xsl:for-each>
                                <td><xsl:value-of select="totalheure"/></td>
                                <td><xsl:value-of select="taux-avancement"/></td>
                                <td><xsl:value-of select="liste-taches/@nombre"/></td>
                                <td><xsl:value-of select="nombre-personnel/@nb"/></td>
                                  
                             
                            </tr></xsl:for-each>
                    
                        <tr>
                            <td colspan="10"> Projet Terminer</td>
                            <td> <xsl:value-of select="count(projet[@Etat='FAIT'])"/></td>
                        </tr></xsl:for-each>
                        
                    </table>
               
                </xsl:for-each>
                <style>
                    h1 {
                    color: #1c87c9;
                    }
                    h2 {
                    color: green;
                    }
                    
                    p {
                    color: #8ebf42;
                    }
                </style>
                
            </body>
            
        </html>
    </xsl:template>
    
</xsl:stylesheet>